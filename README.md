# Hello GitLab

Uma aplicação em Spring Boot, feita
para testar o GitLab, e também os branches do Git.

## Instalação

0. Torne o arquivo "mvnw" executável. (Talvez não precise):
```sh
chmod -x mvnw
```

1. Rode o Spring Boot:
```sh
./mvnw spring-boot:run
```

2. Navegue para a o link [localhost:8080](localhost:8080).


## Compilação

Para compilar a aplicação, sem a executar, rode o comando:

```sh
mvn package
```

### Arquivo CI do Gilab

Ver `.gitlab-ci.yml` para ver como se faz o CI.