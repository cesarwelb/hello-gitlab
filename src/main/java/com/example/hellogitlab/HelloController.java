package com.example.hellogitlab;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/")
    public String hello() {
        return "E aí, Clinio";
    }

    @GetMapping("/red")
    public String red() {
        return "Hello from the red branch";
    }

    @GetMapping("/blue")
    public String blue() {
        return "Hello from blue";
    }
    
}
