package com.example.hellogitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloGitlabApplication.class, args);
	}

}
