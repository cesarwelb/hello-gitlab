package com.example.hellogitlab;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class HelloControllerTest {

  @Test
  public void hello() {
    var controller = new HelloController();
    String response = controller.hello();

    assertEquals(response, "E aí, Clinio");
  }

  @Test
  public void red() {
    var controller = new HelloController();
    String response = controller.red();

    assertEquals(response, "Hello from the red branch");
  }

  @Test
  public void blue() {
    var controller = new HelloController();
    String response = controller.blue();

    assertEquals(response, "Hello from blue");
  }
  
}
